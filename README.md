# Element Trumpf

A web application project by Nana~.

## Setup

This software requires

- [Pyrthon 3.7](https://www.python.org/downloads/)
- [pipenv](https://pipenv.readthedocs.io/en/latest/)
- [Node.js](https://nodejs.org/en/) and
- [yarn](https://yarnpkg.com/en/).

Make shure that this software is installed on your machine. Run all following commands from within the project root directory.

1. Install Python dependencies with pipenv:
    ```shell
    $ pipenv install
    ```
2. Install Javascript dependencies with yarn:
    ```shell
    $ yarn install
    ```
3. Run webpack to build the client:
    ```shell
    $ npx webpack
    ```
4. Run the Flask server:
    ```shell
    $ pipenv run flask run
    ```
5. Browse to `http://localhost:5000/`

6. Have fun!
