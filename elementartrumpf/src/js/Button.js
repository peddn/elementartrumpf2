import * as PIXI from 'pixi.js';

export default class Button extends PIXI.Text {

    constructor(id, text, style, playerCard, enemyCard) {
        super(text, style);
        this.id = id;
        this.interactive = true;
        this.buttonMode = true;
        this.on('click', this.clickHandler);
        this.playerCard = playerCard;
        this.enemyCard = enemyCard;
    }

    clickHandler(event) {
        switch(this.id) {
            case 'size':
                if(this.playerCard.size < this.enemyCard.size) {
                    console.log('verloren')
                } else {
                    console.log('gewonnen');
                }
                break;
            case 'initiative':
                if(this.playerCard.initiative < this.enemyCard.initiative) {
                    console.log('verloren')
                } else {
                    console.log('gewonnen');
                }
                break;
            case 'strength':
                if(this.playerCard.strength < this.enemyCard.strength) {
                    console.log('verloren')
                } else {
                    console.log('gewonnen');
                }
                break;
        }
    }

}