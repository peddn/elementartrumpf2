import * as PIXI from 'pixi.js';

export default class Card extends PIXI.Sprite {
	constructor(data) {
    super(PIXI.loader.resources['static/img/' + data.id + '.png'].texture)
  	this.id = data.id;
    this.name = data.name;
    this.size = data.size;
    this.initiative = data.initiative;
    this.strength = data.strength;
  }

}