import { Application } from 'pixi.js';

import config from './data/game.json';

config.backgroundColor = parseInt(config.backgroundColor);

export default new Application(config);
