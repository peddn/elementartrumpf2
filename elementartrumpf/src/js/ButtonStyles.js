import { TextStyle } from 'pixi.js';

import styles from './data/buttonStyles.json';

class ButtonStyles {
    constructor() {
        for(let style in styles) {
            this[style] = new TextStyle(styles[style]);
        }
    }

    get(styleName) {
        let style = this[styleName];
        if(style) {
            return style;
        } else {
            return new TextStyle({
                fontFamily: 'Arial',
                fontSize: 12
            });
        }
    }
}

const instance = new ButtonStyles();
Object.freeze(instance);

export default instance;