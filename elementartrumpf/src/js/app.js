import * as PIXI from 'pixi.js';

import game from './game';

import cards from './data/cards.json';

import Card from './Card';
import Button from './Button';
import ButtonStyles from './ButtonStyles';

//Create a Pixi Application
// let app = new PIXI.Application({
//     width: 1024, 
//     height: 576,                       
//     antialias: true, 
//     transparent: false, 
//     resolution: 1,
//     backgroundColor: 0xFFFFFF
//   }
// );

//Add the canvas that Pixi automatically created for you to the HTML document
document.getElementById('game').appendChild(game.view);

let state;
let deck = [];

let playerCard;
let enemyCard;

let sizeButton;
let strengthButton;
let initiativeButton;

cards.forEach((card) => {
  PIXI.loader.add('static/img/' + card.id + '.png');
});

//load run the `setup` function when it's done
PIXI.loader.load(setup);

//This `setup` function will run when the image has loaded
function setup() {
  
  // fill the deck with cards
  cards.forEach((card) => {
    deck.push(new Card(card));
  })
  
  reset();
  createButtons();

  //Set the game state
  state = play;
 
  //Start the game loop 
  game.ticker.add(delta => gameLoop(delta));

}

function createButtons() {
  sizeButton = new Button('size', 'Größe', ButtonStyles.get('game'), playerCard, enemyCard);
  sizeButton.x = 100;
  sizeButton.y = 400;
  game.stage.addChild(sizeButton);

  strengthButton = new Button('strength', 'Stärke', ButtonStyles.get('game'), playerCard, enemyCard);
  strengthButton.x = game.renderer.width/2 - strengthButton.width/2;
  strengthButton.y = 400;
  game.stage.addChild(strengthButton);

  initiativeButton = new Button('initiative', 'Initiative', ButtonStyles.get('game'), playerCard, enemyCard);
  initiativeButton.x = game.renderer.width-100-initiativeButton.width;
  initiativeButton.y = 400;
  game.stage.addChild(initiativeButton);
}

function reset() {
  game.stage.removeChild(playerCard);
  game.stage.removeChild(enemyCard);
  if(playerCard !== undefined) {
    deck.push(playerCard);
  }
  if(enemyCard !== undefined) {
    deck.push(enemyCard);
  }
  shuffle();
  playerCard = deck.pop();
  enemyCard = deck.pop();
  playerCard.x = 50;
  playerCard.y = 30;
  enemyCard.x = game.renderer.width - 50 - enemyCard.width;
  enemyCard.y = 30;
  game.stage.addChild(playerCard);
  game.stage.addChild(enemyCard);
}


function gameLoop(delta){

  //Update the current game state:
  state(delta);

}

function play(delta) {
    
}


function shuffle() {
  for (let i = deck.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [deck[i], deck[j]] = [deck[j], deck[i]];
  }
}

function draw() {
  return deck.pop();
}