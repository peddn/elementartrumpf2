const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './elementartrumpf/src/js/app.js',
  devtool: 'source-map',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'elementartrumpf', 'static', 'js')
  },
  plugins: [
    new CopyPlugin([
      //{ from: 'node_modules/p5/lib/p5.min.js', to: 'lib' }
    ]),
  ]
};
